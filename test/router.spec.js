const fetch = require("node-fetch"); //+ npm install node-fetch@2, muuten tuli: RefernceError: fetch is not defined

const expect = require("chai").expect;
const router = require("../src/router");
const PORT = 3000;
const HOST = "127.0.0.1";
//SCHEME or PROTOCOL
const baseUrl = `http://${HOST}:${PORT}`;

describe("Test routes", () => {
    let server;
    before("Start server", (done) => {
        server = router.listen(
            PORT, 
            HOST,
            () => done()
        );
    });
    describe("Routes", () => {
        it("can GET welcome message", async () => {
            const response = await fetch(baseUrl + "/");
            const response_msg = await response.text();
            expect(response_msg).to.equal("welcome!");
        });
        it("Can GET sum of two numbers", async () => {
            const endpoint = baseUrl + "/add";
            const getResponse = (a, b) => new Promise(async (resolve, reject) => {
                const query = `${endpoint}?a=${a}&b=${b}`; //http://example.com/endpoint=a=1&b=2
                const response = await(await fetch(query)).text();
                resolve(response);
            });
            //console.log(await getResponse(1, 2));
            expect(await getResponse(1,2)).to.equal("3");
            console.log(await getResponse("e", "f"));
            console.log(await getResponse(Number.MAX_VALUE, 1));
            console.log(await getResponse(999_999_999_999_999_999_999, 1));
        });
    })
});